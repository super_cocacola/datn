﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using DemoObserver;

public class SpinWheel : MonoBehaviour
{
    public List<int> prize;
    public List<AnimationCurve> animationCurves;

    private bool spinning;
    private float anglePerItem;
    private int randomTime;
    private int itemNumber;
    public bool isMoving;

    private PhotonView view;
    int chessNum;

    void Start()
    {
        spinning = false;
        anglePerItem = 360 / prize.Count;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !spinning && !isMoving)
        {

            randomTime = Random.Range(1, 2);
            itemNumber = Random.Range(0, prize.Count);
            float maxAngle = 360 * randomTime + (itemNumber * anglePerItem);

            StartCoroutine(SpinTheWheel(randomTime, maxAngle));
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            chessNum = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            chessNum = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            chessNum = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            chessNum = 3;
        }
    }

    IEnumerator SpinTheWheel(float time, float maxAngle)
    {
        spinning = true;

        float timer = 0.0f;
        float startAngle = transform.eulerAngles.z;
        maxAngle = maxAngle - startAngle;

        int animationCurveNumber = Random.Range(0, animationCurves.Count);
        Debug.Log("Animation Curve No. : " + animationCurveNumber);

        while (timer < time)
        {
            //to calculate rotation
            float angle = maxAngle * animationCurves[animationCurveNumber].Evaluate(timer / time);
            transform.eulerAngles = new Vector3(0.0f, 0.0f, angle + startAngle);
            timer += Time.deltaTime;
            yield return 0;
        }

        transform.eulerAngles = new Vector3(0.0f, 0.0f, maxAngle + startAngle);
        spinning = false;

        Debug.Log("Get Point: " + (prize[itemNumber] + 1));
        // use prize[itemNumnber] as per requirement
        PhotonView[] views = FindObjectsOfType<PhotonView>();
        for (int i = 0; i < views.Length; i++)
        {
            if (views[i].IsMine)
            {
                view = views[i];
            }
        }
        Move((prize[itemNumber] % (prize.Count) + 1));
        //view.RPC("Move", RpcTarget.AllViaServer, (prize[itemNumber] % (prize.Count) + 1));
        //FindObjectOfType<ChessScript>().Move((prize[itemNumber] % (prize.Count) + 1));
    }

    [PunRPC]
    void Move(int step)
    {
        //view.GetComponentInChildren<ChessScript>().Move((prize[itemNumber] % (prize.Count) + 1));
        GameController.instance.PlayerMove(step, chessNum);
    }
}
