using UnityEngine;
using SS.View;
using System.Collections;

public class MainController : Controller
{
    public const string MAIN_SCENE_NAME = "Main";

    public override string SceneName()
    {
        return MAIN_SCENE_NAME;
    }

    void Awake()
    {
#if UNITY_ANDROID
        if (SystemInfo.systemMemorySize < 2048)
        {
            QualitySettings.SetQualityLevel(1);
        }
#endif
    }

    IEnumerator Start()
    {
        AudioListener.volume = PlayerPrefs.GetInt("GAME_VOLUME", 1);
        if (AudioManager.instance != null)
        {
            AudioManager.instance.PlayBgm("bgm");
        }

        Manager.SceneAnimationDuration = 0.3f;
        Manager.ShieldColor = new Color(0f, 0f, 0f, .85f);
        //Manager.LoadingSceneName = LoadingController.LOADING_SCENE_NAME;

        //Manager.LoadingAnimation(true);
        //Manager.LoadingAnimation(false);

        yield return 0;
        Static.GoMenu();
    }
}