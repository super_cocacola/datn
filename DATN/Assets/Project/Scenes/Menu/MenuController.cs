using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SS.View;
using UnityEngine.UI.Extensions.Examples.FancyScrollViewExample03;
using System.Linq;

public class MenuController : Controller
{
    public const string MENU_SCENE_NAME = "Menu";

    public override string SceneName()
    {
        return MENU_SCENE_NAME;
    }
}