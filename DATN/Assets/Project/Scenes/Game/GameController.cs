using UnityEngine;
using SS.View;
using UnityEngine.SceneManagement;
using Photon.Pun.Demo.Asteroids;
using Photon.Pun;
using DemoObserver;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class GameController : Controller
{
    public const string GAME_SCENE_NAME = "Game";
    public static GameController instance;
    bool chatSceneEnabled = false;
    public static bool hasChat;
    public List<PlayerScript> players = new List<PlayerScript>();
    PlayerScript activePlayer;
    int playerNum;
    public static int num;
    [SerializeField] TextMeshProUGUI status;
    public int level = 0;

    public override string SceneName()
    {
        return GAME_SCENE_NAME;
    }

    private void Awake()
    {
        instance = this;
        PlayerPrefs.SetString("NamePickUserName", PhotonNetwork.LocalPlayer.NickName);
        level = PlayerPrefs.GetInt("LEVEL", 0);
        Instantiate<GameObject>(Resources.Load<GameObject>("Levels/Level " + level), Vector3.zero, Quaternion.identity);
        switch (level)
        {
            case 1:
            case 2:
            case 3:
                {
                    Camera.main.orthographicSize = 5f;
                    break;
                }
            case 4:
            case 5:
                {
                    Camera.main.orthographicSize = 6f;
                    break;
                }
            default:
                break;
        }
    }

    private void Start()
    {
        this.RegisterListener(EventID.OnDoneMove, (param) => NextPlayer());
        activePlayer = players[0];
        playerNum = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (chatSceneEnabled)
            {
                SceneManager.UnloadSceneAsync("DemoChat-Scene");
                chatSceneEnabled = false;
            }
            else
            {
                SceneManager.LoadSceneAsync("DemoChat-Scene", LoadSceneMode.Additive);
                chatSceneEnabled = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.Instance.EndOfGame();
        }
    }

    [PunRPC]
    public void NextPlayer()
    {
        playerNum++;
        activePlayer = players[playerNum % players.Count];
        //SetStatus("Player " + ((playerNum % players.Count) + 1) + " turn");
    }

    public void PlayerMove(int step, int num)
    {
        activePlayer.Move(step, num);
    }

    public void SetStatus(string content)
    {
        status.text = content;
    }
}