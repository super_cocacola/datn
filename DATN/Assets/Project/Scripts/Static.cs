﻿using UnityEngine;
using SS.View;

public static class Static
{
    public static void GoHome()
    {
        Manager.Load(GameController.GAME_SCENE_NAME);
    }
    public static void GoMenu()
    {
        Manager.Load(MenuController.MENU_SCENE_NAME);
    }
    public static void Go(string name, int level = 0)
    {
        Manager.Load(name, level + 1);
    }
    public static void Add(string name)
    {
        Manager.Load(name);
    }
}
