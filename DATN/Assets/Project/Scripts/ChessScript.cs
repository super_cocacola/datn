using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Photon.Pun;
using UnityEngine.Events;
using DemoObserver;

public class Chess
{
    int m_id;
    string m_name;
    Vector3 m_pos;
}

public class ChessScript : MonoBehaviour
{
    Renderer rend;
    bool isEnd;
    bool moveable = true;
    int currentPos;
    int desPos;
    bool isMoving;
    Vector3 basePos;
    bool isBase;
    Collider2D col;
    bool breakMove;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider2D>();
        col.enabled = false;
        basePos = transform.position;
        isBase = true;
    }

    [PunRPC]
    public void Move(int des)
    {
        StartCoroutine(CoMove(des));
    }

    IEnumerator CoMove(int num)
    {
        FindObjectOfType<SpinWheel>().isMoving = true;
        desPos = currentPos + num;
        if (moveable)
        {
            if (!isEnd)
            {
                while (currentPos < desPos)
                {
                    if (breakMove)
                    {
                        breakMove = false;
                        yield break;
                    }
                    isMoving = true;
                    currentPos++;
                    transform.DOMove(LevelController.instance.map[currentPos].position, 0.5f).OnComplete(() =>
                    {
                        isBase = false;
                        col.enabled = true;
                    });
                    yield return new WaitForSeconds(0.5f);
                    if (currentPos == LevelController.instance.map.Count - 1)
                    {
                        isEnd = true;
                        break;
                    }
                }
                isMoving = false;
                currentPos = desPos;
            }
            else
            {
                isMoving = true;
                transform.DOMove(LevelController.instance.minimap[(num % 5) - 1].position, 0.5f).OnComplete(() =>
                {
                    LevelController.instance.CompleteLevel();
                    isMoving = false;
                });
                moveable = false;
            }
        }
        this.PostEvent(EventID.OnDoneMove);
        FindObjectOfType<SpinWheel>().isMoving = false;
        yield break;
    }

    public void SetColor(Material mat)
    {
        rend.material = mat;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Boom"))
        {
            breakMove = true;
            if (!isBase)
            {
                RPC_MoveToBase();
                isBase = true;
                col.enabled = false;
                StopCoroutine("CoMove");
            }
        }
        if (other.CompareTag("Chance"))
        {

        }
    }

    [PunRPC]
    public void RPC_MoveToBase()
    {
        transform.position = basePos;
    }
}
