using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerScript : MonoBehaviour
{
    [SerializeField] ChessScript[] chess;
    public int id;

    public void Move(int step, int num = 0)
    {
        chess[num].Move(step);
    }
}
