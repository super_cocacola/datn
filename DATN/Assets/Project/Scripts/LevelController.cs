using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LevelController : MonoBehaviour
{
    [HideInInspector] public List<Transform> map;
    [HideInInspector] public List<Transform> minimap;
    public Transform[] map1, map2, map3, map4;
    public Transform[] minimap1, minimap2, minimap3, minimap4;
    public Transform[] pos;
    public static LevelController instance;
    PlayerScript[] players;
    int currentPlayer;

    private void Awake()
    {
        instance = this;
        map.Clear();
        minimap.Clear();
        switch (PhotonNetwork.LocalPlayer.ActorNumber)
        {
            case 1:
                {
                    minimap.AddRange(minimap1);
                    map.AddRange(map1);
                    map.AddRange(map2);
                    map.AddRange(map3);
                    map.AddRange(map4);
                    break;
                }
            case 2:
                {
                    minimap.AddRange(minimap2);
                    map.AddRange(map2);
                    map.AddRange(map3);
                    map.AddRange(map4);
                    map.AddRange(map1);
                    break;
                }
            case 3:
                {
                    minimap.AddRange(minimap3);
                    map.AddRange(map3);
                    map.AddRange(map4);
                    map.AddRange(map1);
                    map.AddRange(map2);
                    break;
                }
            case 4:
                {
                    minimap.AddRange(minimap4);
                    map.AddRange(map4);
                    map.AddRange(map1);
                    map.AddRange(map2);
                    map.AddRange(map3);
                    break;
                }
            default:
                {
                    minimap.AddRange(minimap1);
                    map.AddRange(map1);
                    map.AddRange(map2);
                    map.AddRange(map3);
                    map.AddRange(map4);
                    Debug.LogError("default case");
                    break;
                }
        }
    }

    public void CompleteLevel()
    {
        Static.Add(PopupController.POPUP_SCENE_NAME);
    }

    void ChangePlayer()
    {

    }
}
